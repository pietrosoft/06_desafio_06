import { promises } from 'fs'

class Producto{
    constructor (id=0,title,price,thumbnail){
        this.id=id
        this.title=title
        this.price=price
        this.thumbnail=thumbnail
    }
}



class Contenedor{

    constructor (nombre){
        this.nombre=nombre
        this.productos=[]
        
    }
    
    async delete(){
        try{
            await promises.unlink('./'+this.nombre)
            console.log("Archivo : "+this.nombre + " Eliminado.")           
        }
        catch(err){
            //tratamiento de error
            console.log("No se puede eliminar el archivo : "+ this.nombre)
        }
    }
    async deleteAll(){      
            const data = []
            try{
                await promises.writeFile('./'+this.nombre,JSON.stringify(this.productos,null,'\t'))
                console.log("Delete ALL -> Archivo : "+ this.nombre + "  reseteado.")         
            }
            catch(err){
                console.log("No se puede grabar el archivo : "+ this.nombre + err)
            }
    }
   
    async getAll(){
        try{       
            const data = await promises.readFile('./'+this.nombre)
            if(data){
            let arrayProductos = JSON.parse(data)
            
            return arrayProductos
            }
            else{
                console.log("No se encontro data en el archivo : "+ this.nombre ) 
                return null
            }
        }
        catch(err){
            console.log("No se encuentra el archivo : "+ this.nombre + err) 
            return null
        }
       
    }


    async getById(id){
        try{       
            this.productos = await this.getAll()
            let found = new Producto()
            if(this.productos.length >0){
                this.productos.forEach(element => {
                    if(element.id == id){
                        found = new Producto(element.id,element.title,element.price,element.thumbnail)
                        //console.log(p)
                        
                    }
                    //console.log("look up id:"+element.id )
                })    
            }
            
            return found
        }
        catch(err){
            console.log("No se puede leer el archivo : "+ this.nombre + err) 
            return -2
        }
       
    }



    async deleteById(id=0) {
        this.productos = await this.getAll()
        let i = 0;
        let found = false

        while (i < this.productos.length) {
            if (this.productos[i].id === id) {
                this.productos.splice(i, 1);
                console.log("eliminando id:" + id)
                found = true
            } else {
                ++i;
            }
        }
        if (found) {
            //GUAROD EL ARCHIVO
            try{
                await promises.writeFile('./'+this.nombre,JSON.stringify(this.productos,null,'\t'))
                console.log("Archivo : "+ this.nombre + " Guardado.") 
                return true      
            }
            catch (err) {
                console.log("No se puede grabar el archivo : " + this.nombre + err)
            }
        } else {
            console.log("no se eliminaron registros con id:["+id+"]")
        }
        return false;
    }

    async deleteAll() {
            //GUAROD EL ARCHIVO
            try{
                this.productos=[]
                await promises.writeFile('./'+this.nombre,JSON.stringify(this.productos,null,'\t'))
                console.log("Archivo : "+ this.nombre + " Guardado.") 
                return true      
            }
            catch (err) {
                console.log("No se puede grabar el archivo : " + this.nombre + err)
            }
       
        return false;
    }



     async save(producto){
        if(producto ){
            this.productos = await this.getAll()
            if(this.productos == null){
                this.productos =[]
            }
            let id=0
            if(this.productos.length>0){
                let lastId =this.productos[this.productos.length -1].id
                //console.log("last id:"+lastId)
                id=lastId+1
            }else{
                id=1 
            }
            console.log("nuevo id : "+id)
            producto.id=id
            this.productos.push(producto)   
        }
        
        try{
            await promises.writeFile('./'+this.nombre,JSON.stringify(this.productos,null,'\t'))
            console.log("Archivo : "+ this.nombre + " Guardado.") 
            return producto.id        
        }
        catch(err){
            console.log("No se puede grabar el archivo : "+ this.nombre + err)
            return -1
        }
    }

   
}

//genero la instancia de Archivo.
let dataFile = new Contenedor('productos.json');
//Elimino el archivo
//await dataFile.delete();

//Genero objeto producto
console.log("----------------------------------------");
let p1 = new Producto(null,"yerba mate chamigo",250,"none");
console.log("Id generado: "+ await dataFile.save(p1));
let p2 = new Producto(null,"yerba mate marolio",350,"none");
console.log("Id generado: "+ await dataFile.save(p2));
let p3 = new Producto(null,"yerba mate playadito",450,"none");
console.log("Id generado: "+ await dataFile.save(p3));
console.log("----------------------------------------")

console.log("getById(2):" + JSON.stringify(await dataFile.getById(2)))
console.log("----------------------------------------")
console.log("deleteById(2):" + await dataFile.deleteById(2));
console.log("----------------------------------------");
console.log("getById(2):" + JSON.stringify(await dataFile.getById(2)))
console.log("----------------------------------------");
console.log("getAll():"+ JSON.stringify(await dataFile.getAll()))
console.log("----------------------------------------")
console.log("deleteAll():"+ JSON.stringify(await dataFile.deleteAll()))
console.log("----------------------------------------")
console.log("getAll():"+ JSON.stringify(await dataFile.getAll()))
console.log("----------------------------------------")
